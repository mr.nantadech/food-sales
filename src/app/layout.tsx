import type { Metadata } from "next"
import { Inter } from "next/font/google"
import "./globals.css"
import { CssBaseline, StyledEngineProvider } from "@mui/material"
import Wrapper from "./components/shared/wrapper/wrapper"

const inter = Inter({ subsets: ["latin"] })

export const metadata: Metadata = {
  title: "Food Sales",
  description: "Food Sales Table Management",
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en">
      <body className={inter.className}>
        <CssBaseline />
        <StyledEngineProvider injectFirst>
          <Wrapper>{children}</Wrapper>
        </StyledEngineProvider>
      </body>
    </html>
  )
}

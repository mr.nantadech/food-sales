"use client"

import axios from "axios"

export const client = axios.create({
  baseURL: process.env.NEXT_PUBLIC_API_URL,
  headers: {
    "Access-Control-Allow-Origin": "*",
  },
})

export default function Template({ children }: { children: React.ReactNode }) {
  return <>
    {children}
  </>
}

import dayjs from "dayjs"

export interface IFoodSales {
  id: number
  order_date: string
  region: string
  city: string
  category: string
  product: string
  quantity: number
  unitprice: number
  totalprice: number
  manage?: string
}

export const initData: IFoodSales = {
  id: 0,
  order_date: dayjs(new Date()).format("MM/DD/YYYY"),
  region: "",
  city: "",
  category: "",
  product: "",
  quantity: 0,
  unitprice: 0,
  totalprice: 0,
  manage: "",
}

interface IColumbLabel {
  label: string
  disName: string
}
export const ColumbLabel: IColumbLabel[] = [
  { label: "id", disName: "ID" },
  { label: "order_date", disName: "Order Date" },
  { label: "region", disName: "Region" },
  { label: "city", disName: "City" },
  { label: "category", disName: "Category" },
  { label: "product", disName: "Product" },
  { label: "quantity", disName: "Quantity" },
  { label: "unitprice", disName: "Unitprice" },
  { label: "totalprice", disName: "Total Price" },
]

import { IFoodSales } from "@/app/models/food-sales.models"
import { client } from "@/app/template"
import CloseIcon from "@mui/icons-material/Close"
import Alert, { AlertColor } from "@mui/material/Alert"
import Autocomplete from "@mui/material/Autocomplete"
import Button from "@mui/material/Button"
import Dialog from "@mui/material/Dialog"
import DialogActions from "@mui/material/DialogActions"
import DialogContent from "@mui/material/DialogContent"
import DialogTitle from "@mui/material/DialogTitle"
import Grid from "@mui/material/Grid"
import IconButton from "@mui/material/IconButton"
import Snackbar from "@mui/material/Snackbar"
import Stack from "@mui/material/Stack"
import TextField from "@mui/material/TextField"
import styled from "@mui/material/styles/styled"
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs"
import { DatePicker } from "@mui/x-date-pickers/DatePicker"
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider"
import dayjs from "dayjs"
import React, { useEffect, useState } from "react"
import { Controller, FieldValues, useForm } from "react-hook-form"

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  "& .MuiDialogContent-root": {
    padding: theme.spacing(2),
  },
  "& .MuiDialogActions-root": {
    padding: theme.spacing(1),
  },
}))

interface FoodSalesDataDialogProps {
  dialogData: IFoodSales | undefined | null
  dialogMode: string
  handleClose: () => void
  open: boolean
  toggleFetchData: () => void
  hardRefresh: () => void
}

const FoodSalesDataDialog: React.FC<FoodSalesDataDialogProps> = ({
  hardRefresh,
  dialogData,
  dialogMode,
  handleClose,
  open,
  toggleFetchData,
}) => {
  const [onSaveAnyModeStatus, setOnSaveAnyModeStatus] = useState(false)
  const [openToast, setOpenToast] = useState(false)
  const [foodsCategory, setFoodsCategory] = useState([])
  const [alertSuccessStatus, setAlertSuccessStatus] = useState(true)
  const { handleSubmit, reset, control } = useForm()

  const fetchFoodCategory = () => {
    client
      .get(`/category`)
      .then((res) => {
        setFoodsCategory(res.data)
      })
      .catch((e) => {})
      .finally(() => {})
  }

  useEffect(() => {
    fetchFoodCategory()
  }, [])

  const handleCloseToast = (
    event?: React.SyntheticEvent | Event,
    reason?: string
  ) => {
    if (reason === "clickaway") {
      return
    }
    setOpenToast(false)
  }

  function alertInfo() {
    const successMessage =
      dialogMode === "Edit"
        ? "Successfully Updated Data!"
        : "Successfully Added Data!"
    const errorMessage =
      dialogMode === "Edit" ? "Failed To Update Data!" : "Failed To Add Data!"
    const severity: AlertColor = alertSuccessStatus ? "success" : "error"
    const message = alertSuccessStatus ? successMessage : errorMessage
    return { message, severity }
  }

  const onNewDataSubmit = (data: FieldValues) => {
    setOnSaveAnyModeStatus(true)
    data.order_date = dayjs(data?.order_date).format("MM/DD/YYYY")
    const { id, ...newData } = data
    client
      .post(`foods`, newData, {
        headers: { "Content-Type": "application/json" },
      })
      .then((res) => {
        setAlertSuccessStatus(true)
        setOpenToast(true)
      })
      .catch((e) => {
        setAlertSuccessStatus(false)
        setOpenToast(true)
      })
      .finally(() => {
        reset()
        toggleFetchData()
        setOnSaveAnyModeStatus(false)
        handleClose()
      })
  }

  const onUpdateDataSubmit = (data: FieldValues) => {
    setOnSaveAnyModeStatus(true)
    data.order_date = dayjs(data?.order_date).format("MM/DD/YYYY")
    client
      .patch(`foods/${data.id}`, data, {
        headers: { "Content-Type": "application/json" },
      })
      .then((res) => {
        setAlertSuccessStatus(true)
        setOpenToast(true)
      })
      .catch((e) => {
        setAlertSuccessStatus(false)
        setOpenToast(true)
      })
      .finally(() => {
        reset()
        toggleFetchData()
        setOnSaveAnyModeStatus(false)
        handleClose()
      })
  }

  useEffect(() => {
    dialogData && reset(dialogData)
  }, [dialogData, reset])

  return (
    <>
      <Snackbar
        anchorOrigin={{ vertical: "top", horizontal: "center" }}
        open={openToast}
        autoHideDuration={3000}
        onClose={handleCloseToast}>
        <Alert
          onClose={handleCloseToast}
          severity={alertInfo().severity}
          sx={{ width: "100%" }}>
          {alertInfo().message}
        </Alert>
      </Snackbar>

      <BootstrapDialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}>
        <DialogTitle sx={{ m: 0, p: 2 }} id="customized-dialog-title">
          {dialogMode} Data
        </DialogTitle>
        <IconButton
          aria-label="close"
          onClick={handleClose}
          disabled={onSaveAnyModeStatus}
          sx={{
            position: "absolute",
            right: 8,
            top: 8,
            color: (theme) => theme.palette.grey[500],
          }}>
          <CloseIcon />
        </IconButton>
        <form
          onSubmit={handleSubmit(
            dialogMode == "Edit" ? onUpdateDataSubmit : onNewDataSubmit
          )}>
          <DialogContent dividers>
            <Grid container spacing={2}>
              {dialogMode == "Edit" ? (
                <Grid item xs={12}>
                  <Controller
                    name="id"
                    control={control}
                    render={({ field }) => (
                      <TextField
                        size="small"
                        fullWidth
                        label="ID"
                        variant="outlined"
                        InputProps={{
                          readOnly: true,
                        }}
                        {...field}
                      />
                    )}
                  />
                </Grid>
              ) : (
                ""
              )}
              <Grid item xs={12}>
                <Stack direction="row" spacing={2}>
                  <Grid item xs={6}>
                    <Controller
                      name="order_date"
                      control={control}
                      render={({ field }) => (
                        <LocalizationProvider dateAdapter={AdapterDayjs}>
                          <DatePicker
                            label="Order Date"
                            format="MM/DD/YYYY"
                            {...field}
                            slotProps={{
                              textField: { size: "small", fullWidth: true },
                            }}
                            value={field.value ? dayjs(field.value) : dayjs()}
                          />
                        </LocalizationProvider>
                      )}
                    />
                  </Grid>
                  <Grid item xs={6}>
                    <Controller
                      name="region"
                      control={control}
                      render={({ field }) => (
                        <TextField
                          size="small"
                          fullWidth
                          label="Region"
                          variant="outlined"
                          {...field}
                        />
                      )}
                    />
                  </Grid>
                </Stack>
              </Grid>
              <Grid item xs={12}>
                <Stack direction="row" spacing={2}>
                  <Grid item xs={6}>
                    <Controller
                      name="city"
                      control={control}
                      render={({ field }) => (
                        <TextField
                          size="small"
                          fullWidth
                          label="City"
                          variant="outlined"
                          {...field}
                        />
                      )}
                    />
                  </Grid>
                  <Grid item xs={6}>
                    <Controller
                      name="category"
                      control={control}
                      render={({ field }) => (
                        <Autocomplete
                          size="small"
                          {...field}
                          disablePortal
                          id="new-autocomplete"
                          options={foodsCategory.map(
                            (option: any) => option.category
                          )}
                          onChange={(event: any, newValue: string | null) => {
                            field.onChange(newValue)
                          }}
                          value={field.value ? field.value : null}
                          renderOption={(props, option) => {
                            return (
                              <li {...props} key={option}>
                                {option}
                              </li>
                            )
                          }}
                          renderInput={(params) => (
                            <TextField
                              {...params}
                              label="Category"
                              required={true}
                            />
                          )}
                        />
                      )}
                    />
                  </Grid>
                </Stack>
              </Grid>
              <Grid item xs={12}>
                <Stack direction="row" spacing={2}>
                  <Grid item xs={6}>
                    <Controller
                      name="product"
                      control={control}
                      render={({ field }) => (
                        <TextField
                          size="small"
                          fullWidth
                          label="Product"
                          variant="outlined"
                          {...field}
                        />
                      )}
                    />
                  </Grid>
                  <Grid item xs={6}>
                    <Controller
                      name="quantity"
                      control={control}
                      render={({ field }) => (
                        <TextField
                          size="small"
                          fullWidth
                          type="number"
                          label="Quantity"
                          variant="outlined"
                          {...field}
                        />
                      )}
                    />
                  </Grid>
                </Stack>
              </Grid>
              <Grid item xs={12}>
                <Stack direction="row" spacing={2}>
                  <Grid item xs={6}>
                    <Controller
                      name="unitprice"
                      control={control}
                      render={({ field }) => (
                        <TextField
                          size="small"
                          fullWidth
                          type="number"
                          label="Unit Price"
                          variant="outlined"
                          {...field}
                        />
                      )}
                    />
                  </Grid>
                  <Grid item xs={6}>
                    <Controller
                      name="totalprice"
                      control={control}
                      render={({ field }) => (
                        <TextField
                          size="small"
                          fullWidth
                          type="number"
                          label="Total Price"
                          variant="outlined"
                          {...field}
                        />
                      )}
                    />
                  </Grid>
                </Stack>
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <Button
              fullWidth
              variant="outlined"
              onClick={handleClose}
              disabled={onSaveAnyModeStatus}>
              Close
            </Button>
            {dialogMode === "New" ? (
              <Button fullWidth variant="outlined" type="submit">
                {onSaveAnyModeStatus ? "Adding..." : "Add"}
              </Button>
            ) : (
              <Button fullWidth variant="contained" type="submit">
                {onSaveAnyModeStatus ? "Saving..." : "Save"}
              </Button>
            )}
          </DialogActions>
        </form>
      </BootstrapDialog>
    </>
  )
}

export default FoodSalesDataDialog

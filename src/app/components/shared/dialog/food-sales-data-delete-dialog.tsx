import Button from "@mui/material/Button"
import Dialog from "@mui/material/Dialog"
import DialogActions from "@mui/material/DialogActions"
import DialogTitle from "@mui/material/DialogTitle"
import React from "react"

function FoodSalesDataDeleteDialog({
  handleDeleteConfirmed,
  handleDeleteConfirmDialogClose,
  idToDelete,
  open,
}: {
  idToDelete: number | undefined | string
  handleDeleteConfirmed: () => void
  handleDeleteConfirmDialogClose: () => void
  open: boolean
}) {
  return (
    <Dialog
      open={open}
      onClose={handleDeleteConfirmDialogClose}
      aria-labelledby="data-food-confirm-dialog-title"
      aria-describedby="data-food-confirm-dialog-description">
      <DialogTitle id="data-food-confirm-dialog-title">
        {`Do you want to delete the data with ID ${idToDelete} ?`}
      </DialogTitle>
      <DialogActions>
        <Button
          className="bg-red-600"
          variant="contained"
          color="error"
          onClick={handleDeleteConfirmed}>
          Yes
        </Button>
        <Button
          variant="outlined"
          onClick={handleDeleteConfirmDialogClose}
          autoFocus>
          No
        </Button>
      </DialogActions>
    </Dialog>
  )
}

export default FoodSalesDataDeleteDialog

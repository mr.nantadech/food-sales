"use client"

import Container from "@mui/material/Container"
import { ReactNode } from "react"

const Wrapper: React.FC<{ children: ReactNode }> = ({ children }) => {
  return (
    <Container
      maxWidth="lg"
      className="d-flex bg-white my-2 p-4 rounded-md shadow">
      {children}
    </Container>
  )
}

export default Wrapper

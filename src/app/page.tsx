"use client"
import DeleteOutlineOutlinedIcon from "@mui/icons-material/DeleteOutlineOutlined"
import EditOutlinedIcon from "@mui/icons-material/EditOutlined"
import Box from "@mui/material/Box"
import Button from "@mui/material/Button"
import Divider from "@mui/material/Divider"
import FormControl from "@mui/material/FormControl"
import Grid from "@mui/material/Grid"
import InputLabel from "@mui/material/InputLabel"
import MenuItem from "@mui/material/MenuItem"
import Paper from "@mui/material/Paper"
import Select, { SelectChangeEvent } from "@mui/material/Select"
import Table from "@mui/material/Table"
import TableBody from "@mui/material/TableBody"
import TableCell from "@mui/material/TableCell"
import TableContainer from "@mui/material/TableContainer"
import TableHead from "@mui/material/TableHead"
import TablePagination from "@mui/material/TablePagination"
import TableRow from "@mui/material/TableRow"
import TableSortLabel from "@mui/material/TableSortLabel"
import TextField from "@mui/material/TextField"
import { visuallyHidden } from "@mui/utils"
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs"
import { DatePicker } from "@mui/x-date-pickers/DatePicker"
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider"
import dayjs, { Dayjs } from "dayjs"
import React, { useEffect, useMemo, useState } from "react"
import FoodSalesDataDeleteDialog from "./components/shared/dialog/food-sales-data-delete-dialog"
import FoodSalesDataDialog from "./components/shared/dialog/food-sales-data-dialog"
import { ColumbLabel, IFoodSales, initData } from "./models/food-sales.models"
import { client } from "./template"

function descendingComparator<T>(a: T, b: T, orderBy: keyof T) {
  if (b[orderBy] < a[orderBy]) {
    return -1
  }
  if (b[orderBy] > a[orderBy]) {
    return 1
  }
  return 0
}

type Order = "asc" | "desc"

function getComparator<Key extends keyof any>(
  order: Order,
  orderBy: Key
): (
  a: { [key in Key]: number | string },
  b: { [key in Key]: number | string }
) => number {
  return order === "desc"
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy)
}

function stableSort<T>(
  array: readonly T[],
  comparator: (a: T, b: T) => number
) {
  const stabilizedThis = array.map((el, index) => [el, index] as [T, number])
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0])
    if (order !== 0) {
      return order
    }
    return a[1] - b[1]
  })
  return stabilizedThis.map((el) => el[0])
}

interface HeadCell {
  id: keyof IFoodSales
  label: string
  numeric: boolean
}

const headCells: readonly HeadCell[] = [
  {
    id: "id",
    numeric: true,
    label: "ID",
  },
  {
    id: "order_date",
    numeric: false,
    label: "Order Date",
  },
  {
    id: "region",
    numeric: false,
    label: "Region",
  },
  {
    id: "city",
    numeric: false,
    label: "City",
  },
  {
    id: "category",
    numeric: false,
    label: "Category",
  },
  {
    id: "product",
    numeric: false,
    label: "Product",
  },
  {
    id: "quantity",
    numeric: true,
    label: "Quantity",
  },
  {
    id: "unitprice",
    numeric: true,
    label: "UnitPrice",
  },
  {
    id: "totalprice",
    numeric: true,

    label: "TotalPrice",
  },
  {
    id: "manage",
    numeric: true,
    label: "",
  },
]

interface EnhancedTableProps {
  onRequestSort: (
    event: React.MouseEvent<unknown>,
    property: keyof IFoodSales
  ) => void
  order: Order
  orderBy: string
}

function EnhancedTableHead(props: EnhancedTableProps) {
  const { order, orderBy, onRequestSort } = props
  const createSortHandler =
    (property: keyof IFoodSales) => (event: React.MouseEvent<unknown>) => {
      onRequestSort(event, property)
    }

  return (
    <TableHead>
      <TableRow>
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            align="center"
            padding="normal"
            sortDirection={orderBy === headCell.id ? order : false}>
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : "asc"}
              onClick={createSortHandler(headCell.id)}>
              {headCell.label}
              {orderBy === headCell.id ? (
                <Box component="span" sx={visuallyHidden}>
                  {order === "desc" ? "sorted descending" : "sorted ascending"}
                </Box>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  )
}

export default function Page() {
  const [filterValue, setFilterValue] = useState("")
  const [dateHaveValue, setDateHaveValue] = useState(false)
  const [filteredData, setFilteredData] = useState([])
  const [selectColumnName, setSelectColumnName] = useState("id")
  const [order, setOrder] = useState<Order>("asc")
  const [orderBy, setOrderBy] = useState<keyof IFoodSales>("id")
  const [page, setPage] = useState(0)
  const [rowsPerPage, setRowsPerPage] = useState(10)
  const [dataFetched, setFetchData] = useState([])
  const [dataFetchedOriginal, setFetchDataOriginal] = useState([])
  const [dataLength, setDataLength] = useState(0)
  const [toggleFetchData, setToggleFetchData] = useState(false)
  const [foodSalesDataDialogOpen, setFoodSalesDataDialogOpen] = useState(false)
  const [dialogMode, setDialogMode] = useState("")
  const [dialogData, setDialogData] = useState<IFoodSales>()
  const [isFetching, setIsFetching] = useState<boolean>(true)
  const [idToDelete, setIdToDelete] = useState<number | string>()
  const [startDate, setStartDate] = useState<Dayjs | null | string>(null)
  const [endDate, setEndDate] = useState<Dayjs | null>(null)
  const [
    foodSalesDataDeleteConfirmDialogOpen,
    setFoodSalesDataDeleteConfirmDialogOpen,
  ] = useState(false)

  const fetchFoodSalesData = () => {
    setIsFetching(true)
    client
      .get<IFoodSales[]>("foods")
      .then((res: any) => {
        setFetchData(res.data)
        setFilteredData(res.data)
        setFetchDataOriginal(res.data)
        setDataLength(res.data.length)
      })
      .finally(() => {
        setIsFetching(false)
      })
  }

  useEffect(() => {
    fetchFoodSalesData()
  }, [])

  useEffect(() => {
    if (toggleFetchData) {
      fetchFoodSalesData()
      setToggleFetchData(false)
    }
  }, [toggleFetchData])

  const visibleRows = useMemo(() => {
    if (dataFetched) {
      return stableSort(
        filterValue || dateHaveValue || filteredData.length > 0
          ? filteredData
          : dataFetched,
        getComparator(order, orderBy)
      ).slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
    }
    return []
  }, [
    dataFetched,
    dateHaveValue,
    filterValue,
    filteredData,
    order,
    orderBy,
    page,
    rowsPerPage,
  ])

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage)
  }

  const handleRequestSort = (
    event: React.MouseEvent<unknown>,
    property: keyof IFoodSales
  ) => {
    const isAsc = orderBy === property && order === "asc"
    setOrder(isAsc ? "desc" : "asc")
    setOrderBy(property)
  }

  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRowsPerPage(parseInt(event.target.value, 10))
    setPage(0)
  }

  const handleDialogEditMode = (id: number | string) => {
    client.get<IFoodSales>(`foods/${id}`).then((res: any) => {
      setDialogData(res.data)
    })
    setDialogMode("Edit")
    setFoodSalesDataDialogOpen(true)
  }

  const handleDialogNewMode = () => {
    setDialogMode("New")
    setFoodSalesDataDialogOpen(true)
  }

  const handleDialogDeleteConfirm = (id: number | string) => {
    setIdToDelete(id)
    setFoodSalesDataDeleteConfirmDialogOpen(true)
  }

  const OnDeleteConfirm = () => {
    client
      .delete(`foods/${idToDelete}`)
      .then(() => {
        setFilteredData((prevData) =>
          prevData.filter((item: any) => item.id !== idToDelete)
        )
      })
      .finally(() => {
        setFoodSalesDataDeleteConfirmDialogOpen(false)
        setToggleFetchData(true)
      })
  }

  const handleFilterChange = (value: string) => {
    if (value.length == 0) {
      setFilteredData(dataFetched)
    }
    setFilterValue(value)
    const updatedFilteredData = dataFetchedOriginal.filter((row: any) => {
      const columnValue = row[selectColumnName].toString()
      return (
        columnValue && columnValue.toLowerCase().includes(value.toLowerCase())
      )
    })
    setFilteredData([...updatedFilteredData])
  }

  const filterData = (start: any, end: any) => {
    setDateHaveValue(true)
    if (start == null && end == null) {
      setFilteredData(dataFetched)
    }

    const dateStart = dayjs(start).format("MM/DD/YYYY")
    const dateEnd = dayjs(end).format("MM/DD/YYYY")

    const filteredData = dataFetched.filter((item: any) => {
      const orderDate = dayjs(item.order_date, "MM/DD/YYYY")
      return (
        (!dateStart || orderDate.isAfter(start) || orderDate.isSame(start)) &&
        (!dateEnd || orderDate.isBefore(end) || orderDate.isSame(end))
      )
    })
    setFilteredData([...filteredData])
  }

  const handleFilterStartDateChange = (
    newValue: React.SetStateAction<string | dayjs.Dayjs | null>
  ) => {
    setStartDate(newValue)
    if (endDate) {
      filterData(newValue, endDate)
    }
  }

  const handleFilterEndDateChange = (
    newValue: React.SetStateAction<dayjs.Dayjs | null>
  ) => {
    setEndDate(newValue)
    if (startDate) {
      filterData(startDate, newValue)
    }
  }

  const handleColChange = (event: SelectChangeEvent) => {
    setSelectColumnName(event.target.value as string)
  }

  const handleRefreshData = () => {
    fetchFoodSalesData()
    filterData(null, null)
    setEndDate(null)
    setStartDate(null)
    setSelectColumnName("id")
    setFilterValue("")
    setFilteredData(dataFetched)
    setPage(0)
  }

  return (
    <Box sx={{ width: "100%" }}>
      <Grid container className="my-5 px-2">
        <Grid item md={6} sm={12} xs={12}>
          <h1 className="text-blue-500 flex h-fit justify-center md:justify-start self-center my-2 text-lg">
            Food Sales Table
          </h1>
        </Grid>
        <Grid
          item
          md={6}
          sm={12}
          xs={12}
          className="flex h-fit sm:justify-end justify-center self-center">
          <Button
            size="small"
            className="min-w-fit mr-2"
            variant="contained"
            id="new-data-button"
            onClick={handleDialogNewMode}>
            Add Data
          </Button>
          <Button
            size="small"
            className="min-w-fit"
            variant="contained"
            id="new-data-button"
            onClick={handleRefreshData}>
            Refresh Data
          </Button>
        </Grid>
      </Grid>
      <Grid container className="my-5 px-2" spacing={2}>
        <Grid item md={6} sm={6} xs={12}>
          <LocalizationProvider dateAdapter={AdapterDayjs}>
            <DatePicker
              label="Start Date"
              value={startDate}
              format="MM/DD/YYYY"
              onChange={(newValue) => handleFilterStartDateChange(newValue)}
              slotProps={{
                textField: { size: "small", fullWidth: true },
              }}
            />
          </LocalizationProvider>
        </Grid>
        <Grid
          item
          md={6}
          sm={6}
          xs={12}
          className="flex h-fit md:justify-end xs:justify-start sm:justify-start self-center">
          <LocalizationProvider dateAdapter={AdapterDayjs}>
            <DatePicker
              label="End Date"
              value={endDate}
              format="MM/DD/YYYY"
              onChange={(newValue) => handleFilterEndDateChange(newValue)}
              slotProps={{
                textField: { size: "small", fullWidth: true },
              }}
            />
          </LocalizationProvider>
        </Grid>
      </Grid>
      <Divider />
      <Grid container className="my-5 px-2" spacing={2}>
        <Grid item md={6} sm={6} xs={12}>
          <FormControl fullWidth>
            <InputLabel id="demo-simple-select-label">
              Select Column name to search
            </InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={selectColumnName}
              label="Select Column name to search"
              size="small"
              onChange={handleColChange}>
              {ColumbLabel.map((option: any) => (
                <MenuItem key={option.label} value={option.label}>
                  {option.disName}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </Grid>
        <Grid
          item
          md={6}
          sm={6}
          xs={12}
          className="flex h-fit md:justify-end xs:justify-start sm:justify-start self-center">
          <TextField
            id="filled-search"
            type="search"
            size="small"
            fullWidth
            label="Text to search"
            variant="outlined"
            value={filterValue}
            onChange={(e) => handleFilterChange(e.target.value)}
          />
        </Grid>
      </Grid>
      <Paper sx={{ width: "100%", mb: 2 }}>
        <TableContainer>
          <Table
            sx={{ minWidth: 750 }}
            aria-labelledby="tableTitle"
            size="medium">
            <EnhancedTableHead
              order={order}
              orderBy={orderBy}
              onRequestSort={handleRequestSort}
            />
            {!visibleRows.length ? (
              <TableBody>
                <TableRow>
                  <TableCell colSpan={10} align="center">
                    {isFetching ? "...Loading" : "Empty Result"}
                  </TableCell>
                </TableRow>
              </TableBody>
            ) : (
              <TableBody>
                {visibleRows.map((row, index) => {
                  const labelId = `enhanced-table-checkbox-${index}`
                  return (
                    <TableRow
                      key={row.id}
                      tabIndex={-1}
                      sx={{ cursor: "pointer" }}>
                      <TableCell
                        align="center"
                        component="th"
                        id={labelId}
                        scope="row"
                        padding="none">
                        {row.id}
                      </TableCell>
                      <TableCell
                        align="center"
                        component="th"
                        id={labelId}
                        scope="row"
                        padding="none">
                        {row.order_date}
                      </TableCell>
                      <TableCell align="center">{row.region}</TableCell>
                      <TableCell align="center">{row.city}</TableCell>
                      <TableCell align="center">{row.category}</TableCell>
                      <TableCell align="center">{row.product}</TableCell>
                      <TableCell align="center">{row.quantity}</TableCell>
                      <TableCell align="center">{row.unitprice}</TableCell>
                      <TableCell align="center">{row.totalprice}</TableCell>
                      <TableCell align="center">
                        <EditOutlinedIcon
                          color="primary"
                          className="cursor-pointer"
                          onClick={() => handleDialogEditMode(row.id)}
                        />
                        <DeleteOutlineOutlinedIcon
                          color="primary"
                          className="cursor-pointer"
                          onClick={() => handleDialogDeleteConfirm(row.id)}
                        />
                      </TableCell>
                    </TableRow>
                  )
                })}
              </TableBody>
            )}
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={
            filterValue || dateHaveValue ? filteredData.length : dataLength
          }
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Paper>
      <FoodSalesDataDialog
        toggleFetchData={() => setToggleFetchData(true)}
        hardRefresh={handleRefreshData}
        dialogData={dialogMode == "Edit" ? dialogData : initData}
        dialogMode={dialogMode}
        handleClose={() => setFoodSalesDataDialogOpen(false)}
        open={foodSalesDataDialogOpen}
      />
      <FoodSalesDataDeleteDialog
        idToDelete={idToDelete}
        handleDeleteConfirmed={OnDeleteConfirm}
        handleDeleteConfirmDialogClose={() =>
          setFoodSalesDataDeleteConfirmDialogOpen(false)
        }
        open={foodSalesDataDeleteConfirmDialogOpen}
      />
    </Box>
  )
}
